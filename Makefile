##
.ONESHELL:
.SHELLFLAGS := -eu  -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
.DELETE_ON_ERROR:

KIBOT ?= podman run --rm -v $(PWD):$(PWD) -w $(PWD) setsoft/kicad_auto kibot
KIBOTFLAGS ?=
OUTPUT_DIR ?= kibot_output/
FAB_OUT ?= $(OUTPUT_DIR)/pcb.zip
PCB := adb_conv.kicad_pcb
SCH := adb_conv.sch



zip: kibot
	rm -f $(FAB_OUT)
	zip -r $(FAB_OUT) $(OUTPUT_DIR)/pcb_output

.PHONY := kibot zip
kibot:
	$(KIBOT) $(KIBOTFLAGS) -b $(PCB) -e $(SCH) -d $(OUTPUT_DIR) -c fab.kibot.yaml
