# Miniture ADB converter
This is an USB converter for old Apple keyboards using the ADB bus. In terms of functionality, it is similar to [the TMK keyboard converter](https://geekhack.org/index.php?topic=72052.0). However, it uses the modern STM32F042 chip instead of the old Atmel one. 
